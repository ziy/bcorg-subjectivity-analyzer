package edu.cmu.cs.ziy.usf;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

public class UsfNormAccess {

  private Map<String, Double> word2qcons;

  public UsfNormAccess() {
    word2qcons = Maps.newHashMap();
  }

  public void loadFile(String usfNormFile) throws IOException {
    List<String> lines = Files.readAllLines(Paths.get(usfNormFile), StandardCharsets.ISO_8859_1);
    for (String line : lines) {
      List<String> result = Splitter.on(',').trimResults().splitToList(line);
      try {
        double qcon = Double.parseDouble(result.get(15));
        word2qcons.put(result.get(0).toLowerCase(), qcon);
      } catch (NumberFormatException e) {
      }
    }
  }

  public Map<String, Double> getWord2qcons() {
    return word2qcons;
  }

  public static void main(String[] args) throws IOException {
    UsfNormAccess una = new UsfNormAccess();
    File dir = new File("data/usf");
    for (File file : dir.listFiles()) {
      una.loadFile(file.getAbsolutePath());
    }
    System.out.println(una.getWord2qcons().keySet().size());
    List<Map.Entry<String, Double>> entries = Lists.newArrayList(una.getWord2qcons().entrySet());
    Collections.sort(entries, new Comparator<Map.Entry<String, Double>>() {

      @Override
      public int compare(Entry<String, Double> o1, Entry<String, Double> o2) {
        return Double.compare(o1.getValue(), o2.getValue());
      }

    });
    System.out.println(entries.subList(0, 10));
    System.out.println(entries.subList(entries.size() - 10, entries.size()));
  }
}
