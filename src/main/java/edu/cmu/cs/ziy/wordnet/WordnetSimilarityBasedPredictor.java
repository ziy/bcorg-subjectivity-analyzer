package edu.cmu.cs.ziy.wordnet;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.math3.stat.StatUtils;

import com.google.common.base.Charsets;
import com.google.common.base.Joiner;
import com.google.common.base.Stopwatch;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.google.common.io.Files;
import com.google.common.primitives.Doubles;

import edu.cmu.lti.lexical_db.NictWordNet;
import edu.cmu.lti.ws4j.RelatednessCalculator;
import edu.cmu.lti.ws4j.impl.WuPalmer;
import edu.stanford.nlp.ling.CoreAnnotations.LemmaAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.PartOfSpeechAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;

public class WordnetSimilarityBasedPredictor {

  private WordnetSimilarityFeatureExtractor extractor;

  private Map<String, Double> examplar2qcon;

  private StanfordCoreNLP pipeline;

  private RelatednessCalculator rc;

  public WordnetSimilarityBasedPredictor(File word2examplarFile, File word2qconFile)
          throws IOException {
    extractor = new WordnetSimilarityFeatureExtractor();
    Set<String> examplars = Sets.newHashSet();
    for (String line : Files.readLines(word2examplarFile, Charsets.UTF_8)) {
      examplars.add(line.split("\t")[1]);
    }
    examplar2qcon = Maps.newHashMap();
    for (String line : Files.readLines(word2qconFile, Charsets.UTF_8)) {
      String[] segs = line.split("\t");
      if (examplars.contains(segs[0])) {
        examplar2qcon.put(segs[0], Double.parseDouble(segs[1]));
      }
    }
    Properties props = new Properties();
    props.put("annotators", "tokenize,ssplit,pos,lemma");
    pipeline = new StanfordCoreNLP(props);
    rc = new WuPalmer(new NictWordNet());
  }

  public List<Double> getSubjectivity(String text) {
    Annotation document = new Annotation(text);
    pipeline.annotate(document);
    List<Double> qcons = Lists.newArrayList();
    for (CoreLabel token : document.get(TokensAnnotation.class)) {
      String pos = token.get(PartOfSpeechAnnotation.class);
      if (!pos.startsWith("N") && !pos.startsWith("V") && !pos.startsWith("R")
              && !pos.startsWith("J")) {
        continue;
      }
      String lemma = token.get(LemmaAnnotation.class);
      double maxSim = Double.NEGATIVE_INFINITY;
      double maxQcon = Double.NaN;
      for (Entry<String, Double> e : examplar2qcon.entrySet()) {
        try {
          double sim = extractor.getSimilarity(lemma, e.getKey(), rc);
          if (sim > maxSim) {
            maxSim = sim;
            maxQcon = e.getValue();
          }
        } catch (Exception ex) {
        }
        qcons.add(maxQcon);
      }
    }
    double[] qconValues = Doubles.toArray(qcons);
    double mean = StatUtils.mean(qconValues);
    return Lists.newArrayList(mean, StatUtils.variance(qconValues, mean),
            StatUtils.min(qconValues), StatUtils.max(qconValues));
  }

  public static void main(String[] args) throws IOException {
    WordnetSimilarityBasedPredictor wsbp = new WordnetSimilarityBasedPredictor(new File(
            "data/cluster/word2examplar"), new File("data/similarity/word2qcon"));
    List<String> lines = Files.readLines(new File(args[0]), Charsets.UTF_8);
    BufferedWriter writer = Files.newWriter(new File(args[1]), Charsets.UTF_8);
    Stopwatch stopwatch = Stopwatch.createStarted();
    int count = 0;
    for (String line : lines) {
      System.out.println(count++ + " " + stopwatch);
      List<Double> values = wsbp.getSubjectivity(line);
      writer.write(Joiner.on(", ").join(values) + "\n");
    }
    writer.close();
  }
}
