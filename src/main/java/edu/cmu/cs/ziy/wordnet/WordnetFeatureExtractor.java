package edu.cmu.cs.ziy.wordnet;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.google.common.base.Charsets;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.google.common.io.Files;

import edu.cmu.cs.ziy.usf.UsfNormAccess;
import edu.mit.jwi.Dictionary;
import edu.mit.jwi.item.IIndexWord;
import edu.mit.jwi.item.IWord;
import edu.mit.jwi.item.IWordID;
import edu.mit.jwi.item.POS;

public class WordnetFeatureExtractor {

  private Dictionary dict;

  public WordnetFeatureExtractor(String path) throws IOException {
    URL url = new URL("file", null, path);
    dict = new Dictionary(url);
    dict.open();
  }

  public String getLexicalType(String lemma, POS pos) {
    IIndexWord idxWord = dict.getIndexWord(lemma, POS.NOUN);
    IWordID wordID = idxWord.getWordIDs().get(0);
    IWord word = dict.getWord(wordID);
    return word.getSynset().getLexicalFile().getName().toLowerCase();
  }

  public Set<String> getLexicalType(String lemma) {
    Set<String> types = Sets.newHashSet();
    try {
      types.add(getLexicalType(lemma, POS.NOUN));
    } catch (Exception e) {
    }
    try {
      types.add(getLexicalType(lemma, POS.ADVERB));
    } catch (Exception e) {
    }
    try {
      types.add(getLexicalType(lemma, POS.VERB));
    } catch (Exception e) {
    }
    try {
      types.add(getLexicalType(lemma, POS.ADJECTIVE));
    } catch (Exception e) {
    }
    return types;
  }

  public void close() {
    dict.close();
  }

  public static void main(String[] args) throws IOException {

    // generate word list
    UsfNormAccess una = new UsfNormAccess();
    File dir = new File("data/usf");
    for (File file : dir.listFiles()) {
      una.loadFile(file.getAbsolutePath());
    }

    // find words in wordnet
    String path = "data/wordnet/dict";
    WordnetFeatureExtractor wfe = new WordnetFeatureExtractor(path);
    Map<String, Integer> type2index = Maps.newHashMap();
    System.out.println(una.getWord2qcons().size());
    Iterator<Entry<String, Double>> it = una.getWord2qcons().entrySet().iterator();
    while (it.hasNext()) {
      Set<String> types = wfe.getLexicalType(it.next().getKey());
      if (types.size() == 0) {
        it.remove();
      }
      for (String t : types) {
        if (!type2index.containsKey(t)) {
          type2index.put(t, type2index.size() + 1);
        }
      }
    }
    System.out.println(una.getWord2qcons().size());

    // write the libsvm format feature file
    BufferedWriter qconWriter = Files.newWriter(new File("data/similarity/word2qcon"),
            Charsets.UTF_8);
    BufferedWriter featureWriter = Files.newWriter(new File("data/liblinear/features"),
            Charsets.UTF_8);
    for (Entry<String, Double> e : una.getWord2qcons().entrySet()) {
      Set<String> types = wfe.getLexicalType(e.getKey());
      assert types.size() > 0;
      featureWriter.write(String.valueOf(e.getValue()));
      for (String t : types) {
        featureWriter.write(" " + type2index.get(t) + ":1");
      }
      featureWriter.write("\n");
      qconWriter.write(e.getKey() + "\t" + e.getValue() + "\n");
    }
    featureWriter.close();
    qconWriter.close();
  }

}
