package edu.cmu.cs.ziy.wordnet;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Map;

import com.google.common.base.Charsets;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.io.Files;

import edu.cmu.lti.lexical_db.ILexicalDatabase;
import edu.cmu.lti.lexical_db.NictWordNet;
import edu.cmu.lti.ws4j.RelatednessCalculator;
import edu.cmu.lti.ws4j.impl.HirstStOnge;
import edu.cmu.lti.ws4j.impl.JiangConrath;
import edu.cmu.lti.ws4j.impl.LeacockChodorow;
import edu.cmu.lti.ws4j.impl.Lesk;
import edu.cmu.lti.ws4j.impl.Lin;
import edu.cmu.lti.ws4j.impl.Path;
import edu.cmu.lti.ws4j.impl.Resnik;
import edu.cmu.lti.ws4j.impl.WuPalmer;
import edu.cmu.lti.ws4j.util.WS4JConfiguration;

public class WordnetSimilarityFeatureExtractor {

  private static ILexicalDatabase db = new NictWordNet();

  private static RelatednessCalculator[] rcs = { new HirstStOnge(db), new LeacockChodorow(db),
      new Lesk(db), new WuPalmer(db), new Resnik(db), new JiangConrath(db), new Lin(db),
      new Path(db) };

  public WordnetSimilarityFeatureExtractor() throws IOException {
    WS4JConfiguration.getInstance().setMFS(true);
  }

  public List<Double> getSimilarity(String word1, String word2) {
    List<Double> sims = Lists.newArrayList();
    for (RelatednessCalculator rc : rcs) {
      double sim = getSimilarity(word1, word2, rc);
      sims.add(sim);
      System.out.println(rc.getClass().getName() + "\t" + sim);
    }
    return sims;
  }

  public double getSimilarity(String word1, String word2, RelatednessCalculator rc) {
    return rc.calcRelatednessOfWords(word1, word2);
  }

  public static void main(String[] args) throws IOException {
    // load word2qcon file
    List<String> lines = Files.readLines(new File("data/similarity/word2qcon"), Charsets.UTF_8);
    Map<String, Double> word2qcon = Maps.newHashMap();
    for (String line : lines) {
      String[] segs = line.split("\t");
      word2qcon.put(segs[0], Double.parseDouble(segs[1]));
    }
    // calculate similarity and write to fle
    WordnetSimilarityFeatureExtractor wsfe = new WordnetSimilarityFeatureExtractor();
    DecimalFormat df = new DecimalFormat("#.####");
    for (String word1 : args) {
      System.out.println(word1);
      BufferedWriter bw = Files.newWriter(
              new File("data/similarity/sim-" + word1.replace(" ", "-")), Charsets.UTF_8);
      for (String word2 : word2qcon.keySet()) {
        bw.write(word1 + "\t" + word2);
        List<Double> sims = wsfe.getSimilarity(word1, word2);
        for (double sim : sims) {
          bw.write("\t" + df.format(sim));
        }
        bw.write("\n");
      }
      bw.close();
    }
  }

}
