package edu.cmu.cs.ziy.bcorg.nlp;

import java.io.File;
import java.io.IOException;
import java.util.List;

import com.google.common.base.Stopwatch;
import com.google.common.collect.Lists;

import edu.cmu.cs.ziy.bcorg.common.BcorgSentenceReader;
import edu.cmu.cs.ziy.bcorg.common.BcorgSentenceReader.BcorgIdSentence;
import edu.washington.cs.knowitall.extractor.ReVerbExtractor;
import edu.washington.cs.knowitall.extractor.conf.ReVerbOpenNlpConfFunction;
import edu.washington.cs.knowitall.nlp.ChunkedSentence;
import edu.washington.cs.knowitall.nlp.OpenNlpSentenceChunker;
import edu.washington.cs.knowitall.nlp.extraction.ChunkedBinaryExtraction;

public class BcorgReberbChunker {

  public static class Relation {

    public String relation;

    public String arg1;

    public String arg2;

    public double confidence;

    public Relation(String relation, String arg1, String arg2, double confidence) {
      super();
      this.relation = relation;
      this.arg1 = arg1;
      this.arg2 = arg2;
      this.confidence = confidence;
    }

    @Override
    public String toString() {
      return "Relation [relation=" + relation + ", arg1=" + arg1 + ", arg2=" + arg2
              + ", confidence=" + confidence + "]";
    }

  }

  private OpenNlpSentenceChunker chunker;

  private ReVerbExtractor reverb;

  private ReVerbOpenNlpConfFunction confFunc;

  public BcorgReberbChunker() throws IOException {
    chunker = new OpenNlpSentenceChunker();
    reverb = new ReVerbExtractor();
    confFunc = new ReVerbOpenNlpConfFunction();
  }

  public List<Relation> chunkSentence(String sentence) {
    ChunkedSentence sent = chunker.chunkSentence(sentence);
    List<Relation> relations = Lists.newArrayList();
    for (ChunkedBinaryExtraction extr : reverb.extract(sent)) {
      relations.add(new Relation(extr.getRelation().toString(), extr.getArgument1().toString(),
              extr.getArgument2().toString(), confFunc.getConf(extr)));
    }
    return relations;
  }

  public static void main(String[] args) throws IOException {
    BcorgReberbChunker brc = new BcorgReberbChunker();
    BcorgSentenceReader bsr = new BcorgSentenceReader(new File[] { new File(
            "data/random-sample.sentences") });
    int lineno = 0;
    Stopwatch stopwatch = Stopwatch.createStarted();
    while (bsr.hasNext()) {
      if (lineno++ % 1000 == 0) {
        System.out.println(lineno / 1000 + "k " + stopwatch);
      }
      BcorgIdSentence bis = bsr.next();
      System.out.println(bis);
      for (Relation relation : brc.chunkSentence(bis.sentence)) {
        System.out.println(" - " + relation);
      }
    }
  }
  
}
