package edu.cmu.cs.ziy.bcorg.nlp;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.List;
import java.util.Properties;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import org.jdom2.JDOMException;

import com.google.common.collect.Lists;

import edu.cmu.cs.ziy.bcorg.common.BcorgPost;
import edu.cmu.cs.ziy.bcorg.common.BcorgPostsXmlReader;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;

public class BcorgSentenceSegmenter {

  private StanfordCoreNLP pipeline;

  public BcorgSentenceSegmenter() {
    Properties props = new Properties();
    props.put("annotators", "tokenize,ssplit");
    pipeline = new StanfordCoreNLP(props);
  }

  public void segmentFiles(File[] files) throws FileNotFoundException, JDOMException, IOException {
    for (File file : files) {
      segmentFile(file);
    }
  }

  private void segmentFile(File file) throws FileNotFoundException, JDOMException, IOException {
    String inputPath = file.getAbsolutePath();
    if (inputPath.endsWith("xml")) {
      String outputPath = inputPath.substring(0, inputPath.length() - 3) + "sentences";
      segmentFile(new BufferedInputStream(new FileInputStream(inputPath)),
              new BufferedOutputStream(new FileOutputStream(outputPath)));
    } else if (inputPath.endsWith("xml.gz")) {
      String outputPath = inputPath.substring(0, inputPath.length() - 6) + "sentences.gz";
      segmentFile(new BufferedInputStream(new GZIPInputStream(new FileInputStream(inputPath))),
              new BufferedOutputStream(new GZIPOutputStream(new FileOutputStream(outputPath))));
    }
  }

  private void segmentFile(InputStream inputStream, OutputStream outputStream)
          throws JDOMException, IOException {
    BcorgPostsXmlReader reader = new BcorgPostsXmlReader(inputStream);
    BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(outputStream));
    while (reader.hasNext()) {
      BcorgPost post = reader.next();
      String id = post.getForumId() + ":" + post.getThreadId() + ":" + post.getId();
      int i = 0;
      for (String sentence : segmentString(post.getContent())) {
        bw.write(id + ":" + i++ + "\t" + sentence.replaceAll("\\s+", " ") + "\n");
      }
    }
    bw.close();
  }

  private List<String> segmentString(String text) {
    Annotation document = new Annotation(text);
    pipeline.annotate(document);
    List<String> sentences = Lists.newArrayList();
    for (CoreMap sentence : document.get(SentencesAnnotation.class)) {
      sentences.add(sentence.toString());
    }
    return sentences;
  }

  public static void main(String[] args) throws FileNotFoundException, JDOMException, IOException {
    BcorgSentenceSegmenter bss = new BcorgSentenceSegmenter();
    File dir = new File("../bcorg-posts-indexer/data/");
    bss.segmentFiles(dir.listFiles());
  }

}
