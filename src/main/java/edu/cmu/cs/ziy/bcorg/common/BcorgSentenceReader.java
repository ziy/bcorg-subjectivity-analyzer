package edu.cmu.cs.ziy.bcorg.common;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.zip.GZIPInputStream;

import com.google.common.base.Charsets;
import com.google.common.collect.ContiguousSet;
import com.google.common.collect.DiscreteDomain;
import com.google.common.collect.Lists;
import com.google.common.collect.Range;
import com.google.common.collect.Sets;
import com.google.common.io.Files;

import edu.cmu.cs.ziy.bcorg.common.BcorgSentenceReader.BcorgIdSentence;

public class BcorgSentenceReader implements Iterator<BcorgIdSentence> {

  public static class BcorgIdSentence {

    public String id;

    public String sentence;

    public BcorgIdSentence(String id, String sentence) {
      super();
      this.id = id;
      this.sentence = sentence;
    }

    @Override
    public String toString() {
      return "BcorgIdSentence [id=" + id + ", sentence=" + sentence + "]";
    }

  }

  private List<InputStream> iss;

  private int fileno;

  public BcorgSentenceReader(File[] files) throws IOException {
    this.iss = new ArrayList<InputStream>();
    for (File file : files) {
      if (file.getAbsolutePath().endsWith("sentences.gz")) {
        iss.add(new GZIPInputStream(new FileInputStream(file)));
      } else if (file.getAbsolutePath().endsWith("sentences")) {
        iss.add(new FileInputStream(file));
      }
    }
    this.fileno = -1;
  }

  private BufferedReader br;

  private String line;

  @Override
  public boolean hasNext() {
    try {
      while (br == null || (line = br.readLine()) == null) {
        fileno++;
        if (fileno == iss.size()) {
          return false;
        }
        br = new BufferedReader(new InputStreamReader(iss.get(fileno)));
      }
      return true;
    } catch (IOException e) {
      return false;
    }
  }

  @Override
  public BcorgIdSentence next() {
    String[] segs = line.split("\t", 2);
    return new BcorgIdSentence(segs[0], segs[1]);
  }

  @Override
  public void remove() {
    // TODO Auto-generated method stub

  }

  private static final int NUM_OF_LINES = 10865788;

  private static final int NUM_OF_SAMPLED_LINES = 10000;

  public static void main(String[] args) throws IOException {
    // random number
    List<Integer> allLinenos = Lists.newArrayList(ContiguousSet.create(Range.open(0, NUM_OF_LINES),
            DiscreteDomain.integers()));
    Collections.shuffle(allLinenos);
    // prepare for output
    int numFinishedLinenos = 0;
    BufferedWriter writer = Files.newWriter(new File("data/sample-10k.sentences"), Charsets.UTF_8);
    BufferedWriter idWriter = Files.newWriter(new File("data/sample-10k.sentences.noid"),
            Charsets.UTF_8);
    int iter = 0;
    while (true) {
      Set<Integer> iterLinenos = Sets.newHashSet(allLinenos.subList(iter * NUM_OF_SAMPLED_LINES,
              (iter + 1) * NUM_OF_SAMPLED_LINES));
      // scan and filter files
      File dir = new File("data/");
      File[] files = dir.listFiles(new FilenameFilter() {

        @Override
        public boolean accept(File dir, String name) {
          return name.endsWith("sentences.gz");
        }
      });
      BcorgSentenceReader bsr = new BcorgSentenceReader(files);
      int lineno = 0;
      while (bsr.hasNext()) {
        if (!iterLinenos.contains(lineno++)) {
          continue;
        }
        BcorgIdSentence ids = bsr.next();
        if (ids.sentence.indexOf(' ') <= 0) {
          continue;
        }
        if (!ids.sentence.endsWith(".") && !ids.sentence.endsWith("?")
                && !ids.sentence.endsWith("!")) {
          continue;
        }
        if (!Character.isAlphabetic(ids.sentence.charAt(ids.sentence.length() - 2))) {
          continue;
        }
        if (ids.sentence.split(" ", 30).length == 30) {
          continue;
        }
        ids.sentence.replace("&quot", "");
        writer.write(ids.id + "\t" + ids.sentence + "\n");
        idWriter.write(ids.sentence + "\n");
        numFinishedLinenos++;
        if (numFinishedLinenos >= NUM_OF_SAMPLED_LINES) {
          System.out.println("Finished " + numFinishedLinenos);
          writer.close();
          idWriter.close();
          return;
        }
      }
      System.out.println("Iter " + iter++ + " finishes " + numFinishedLinenos);
    }
  }
}
