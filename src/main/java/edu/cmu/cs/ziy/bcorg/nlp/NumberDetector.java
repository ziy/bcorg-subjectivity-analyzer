package edu.cmu.cs.ziy.bcorg.nlp;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.common.base.Stopwatch;
import com.google.common.collect.Lists;
import com.google.common.io.Files;

import edu.cmu.cs.ziy.bcorg.common.BcorgSentenceReader;
import edu.cmu.cs.ziy.bcorg.common.BcorgSentenceReader.BcorgIdSentence;
import edu.stanford.nlp.ling.CoreAnnotations.TextAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;

public class NumberDetector {

  private StanfordCoreNLP pipeline;

  public NumberDetector() throws IOException {
    Properties props = new Properties();
    props.put("annotators", "tokenize");
    pipeline = new StanfordCoreNLP(props);
  }

  private static Pattern pattern = Pattern.compile("-?[0-9]+\\.?[0-9]*");

  public List<String> detectNumbers(String sentence) {
    List<String> ret = Lists.newArrayList();
    Annotation document = new Annotation(sentence);
    pipeline.annotate(document);
    for (CoreLabel token : document.get(TokensAnnotation.class)) {
      String word = token.get(TextAnnotation.class);
      Matcher matcher = pattern.matcher(word);
      if (matcher.matches()) {
        ret.add(word);
      }
    }
    return ret;
  }

  public static void main(String[] args) throws IOException {
    NumberDetector nd = new NumberDetector();
    BcorgSentenceReader bsr = new BcorgSentenceReader(new File[] { new File(
            "data/random-sample.sentences") });
    int lineno = 0;
    Stopwatch stopwatch = Stopwatch.createStarted();
    BufferedWriter bw = Files.newWriter(new File("data/random-sample.numbers"),
            StandardCharsets.UTF_8);
    while (bsr.hasNext()) {
      if (lineno++ % 1000 == 0) {
        System.out.println(lineno / 1000 + "k " + stopwatch);
      }
      BcorgIdSentence bis = bsr.next();
      List<String> numbers = nd.detectNumbers(bis.sentence);
      bw.write(numbers.size() + "\n");
      if (numbers.size() > 0) {
        System.out.println(bis);
        for (String number : numbers) {
          System.out.println(" - " + number);
        }
      }
    }
    bw.close();
  }

}
