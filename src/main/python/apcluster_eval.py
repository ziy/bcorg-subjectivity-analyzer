#!/usr/bin/python
import numpy

word2examplar_file = 'data/cluster/word2examplar.txt'
word2qcon_file = 'data/similarity/word2qcon'

word2qcon = dict()
for line in open(word2qcon_file):
  segs = line.strip().split('\t')
  word2qcon[segs[0]] = float(segs[1])

sum = 0
count = 0
words = []
examplars = []
for line in open(word2examplar_file):
  segs = line.strip().split('\t')
  word = segs[0]
  examplar = segs[1]
  error = word2qcon[word] - word2qcon[examplar]
  words.append(word2qcon[word])
  examplars.append(word2qcon[examplar])
  print '%s %s' % (word2qcon[word], word2qcon[examplar])
  sum += error * error
  count += 1
print float(sum) / float(count)
print numpy.corrcoef(words, examplars)
