#!/usr/bin/python
from os import listdir
from os.path import join

NUM_SIMS = 8

FORBIT_LIST = ['hue', 'person']

indir = 'data/similarity/'
outdir = 'data/cluster/'
outfiles = [open(join(outdir, 'sim-merged-p%d' % i), 'w') for i in range(0, NUM_SIMS)]
word2idfile = open('data/cluster/word2id', 'w')
num = 0
word2id = dict()
for infile in [f for f in listdir(indir) if f.startswith('sim-')]:
  print num
  num += 1
  for inline in open(join(indir, infile)):
    segs = inline.strip().split('\t')
    if len(segs) != NUM_SIMS + 2: continue
    if segs[0] in FORBIT_LIST or segs[1] in FORBIT_LIST: continue
    if segs[0] == segs[1]: continue
    for i in range(0, NUM_SIMS):
      if segs[0] in word2id:
        id1 = word2id[segs[0]]
      else:
        id1 = len(word2id) + 1
        word2id[segs[0]] = id1
        word2idfile.write('%s\t%s\n' % (segs[0], id1))
      if segs[1] in word2id:
        id2 = word2id[segs[1]]
      else:
        id2 = len(word2id) + 1
        word2id[segs[1]] = id2
        word2idfile.write('%s\t%s\n' % (segs[1], id2))
      outfiles[i].write('%d %d %s\n' % (id1, id2, segs[2+i]))
for i in range(0, NUM_SIMS): outfiles[i].close()
word2idfile.close()

