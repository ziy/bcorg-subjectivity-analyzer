#!/usr/bin/python
wordid_file = 'data/cluster/word2id'
idx_file = 'data/cluster/cluster-p3-0.0000/idx.txt'

wordid = dict()
for line in open(wordid_file):
  segs = line.strip().split('\t')
  wordid[segs[1]] = segs[0]

id = 1
for line in open(idx_file):
  print '%s\t%s' % (wordid[str(id)], wordid[line.strip()])
  id += 1
