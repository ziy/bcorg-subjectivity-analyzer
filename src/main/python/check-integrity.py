#!/usr/bin/python
from os import listdir
from os.path import join


NUM_SIMS = 8

NUM_WORDS = 2800


def check_line(line):
  segs = line.strip().split('\t')
  if len(segs) != NUM_SIMS + 2: return False
  for seg in segs[2:]:
    try:
      float(seg)
    except ValueError:
      return False
  return True


def check_file(dir, f):
  num_lines = 0
  valid_files = set()
  for l in open(join(dir, f)):
    if check_line(l): num_lines += 1
  if num_lines == NUM_WORDS: valid_files.add(f)
  return valid_files


def collect_files(word2qcon):
  all_files = set()
  for l in open(word2qcon):
    all_files.add('sim-%s' % l.split('\t')[0].replace(' ', '-'))
  return all_files


dir = 'data/similarity/'
word2qcon = 'data/similarity/word2qcon'
all_files = collect_files(word2qcon)
valid_files = set()
for f in listdir(dir):
  for w in check_file(dir, f):
    valid_files.add(w)
print all_files.difference(valid_files)
print valid_files.difference(all_files)
